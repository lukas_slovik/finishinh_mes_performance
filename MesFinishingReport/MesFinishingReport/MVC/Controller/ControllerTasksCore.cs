﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MesFinishingReport.MVC.Controller
{
    public abstract class ControllerTasksCore
    {
        private Guid controllerId;
        private LockedRunnableList runningTasks;
        private void RemoveFromRuningTasks(Runnable taskCompleted)
        {
            this.runningTasks.Remove(taskCompleted);
        }
        private LockedRunnableQueue tasks;
        private LockedRunnableQueue Tasks
        {
            get { return tasks; }
        }
        private LockedRunnableQueue taskResults;
        private LockedRunnableQueue TaskResults { get { return taskResults; } }

        public Guid Id
        {
            get
            {
                return this.controllerId;
            }
        }
        public bool IsStopped
        {
            get
            {
                return this.stopThreads;
            }
        }

        private bool stopThreads;
        public void StopController()
        {
            this.runningTasks.StopAllRunningTasks();
            this.stopThreads = true;
        }
        public ControllerTasksCore()
        {
            this.tasks = new LockedRunnableQueue();
            this.taskResults = new LockedRunnableQueue();
            this.runningTasks = new LockedRunnableList();
            this.stopThreads = false;
            this.controllerId = Guid.NewGuid();
            StartTaskExecution();
            StartTaskResultExecution();
        }
        public void AddTaskResult(Runnable taskResult)
        {
            this.taskResults.Add(taskResult);
        }

        protected void AddTask(Runnable taskResult)
        {
            this.tasks.Add(taskResult);
        }

        private void StartTaskExecution()
        {
            Thread taskListProcess = new Thread(TaskStart);
            taskListProcess.Start();
        }

        private void StartTaskResultExecution()
        {
            Thread resultListProcess = new Thread(TaskResultStart);
            resultListProcess.Start();
        }

        private void TaskStart()
        {
            while (!this.stopThreads)
            {
                Runnable newTask = this.tasks.Get();
                if (newTask != null)
                {
                    this.runningTasks.Add(newTask);
                    newTask.StartTask();
                }
                Thread.Sleep(Properties.Settings.Default.EndlessLoopTimeOut);
            }
        }

        private void TaskResultStart()
        {
            while (!this.stopThreads)
            {
                Runnable newTask = this.taskResults.Get();
                if (newTask != null)
                {
                    newTask.ExecuteResult();
                    this.runningTasks.Remove(newTask);
                }
                Thread.Sleep(Properties.Settings.Default.EndlessLoopTimeOut);
            }
        }
    }
}
