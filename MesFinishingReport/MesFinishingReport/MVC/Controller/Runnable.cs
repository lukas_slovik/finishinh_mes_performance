﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MesFinishingReport.MVC.Controller
{
    public abstract class Runnable
    {
        private Guid takId;
        public Guid TaskId
        {
            get
            {
                return this.takId;
            }
        }
        protected ControllerTasksCore mvcController;
        public void StartTask()
        {
            Thread taskThread = new Thread(RunTask);
            taskThread.Start();
        }
        protected abstract void RunTask();
        public void ExecuteResult()
        {
            Thread resultThread = new Thread(RunResult);
            resultThread.Start();
        }
        protected void TaskCompleted()
        {
            this.mvcController.AddTaskResult(this);
        }
        protected abstract void RunResult();
        public abstract void StopTask();
        public Runnable(ControllerTasksCore mvcController)
        {
            this.mvcController = mvcController;
            this.takId = new Guid();
        }
    }
}
